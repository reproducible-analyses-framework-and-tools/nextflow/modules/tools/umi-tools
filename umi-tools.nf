#!/usr/bin/env nextflow

process umitools_extract {
// Runs trim galore
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - FASTQ Prefix
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq2) - FASTQ 2
//   val parstr - Parameter String
//
// output:
//   tuple => emit: procd_fqs
//     val(pat_name) - Patient Name
//     val(run) - FASTQ Prefix
//     val(dataset) - Dataset
//     path("${dataset}-${pat_name}-${run}*_1*.trimmed.f*q.gz") - Trimmed FASTQ 1
//     path("${dataset}-${pat_name}-${run}*_2*.trimmed.f*q.gz") - Trimmed FASTQ 2
//   path("meta") - Metadata File

// require:
//   FQS
//   params.trim_galore$trim_galore_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'umitools_container'
  label 'umitools'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/umitools"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2), path(white_list)
  val bc_pattern
  val parstr

//  output:
//  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}*_1*.trimmed.f*q.gz"), path("${dataset}-${pat_name}-${run}*_2*.trimmed.f*q.gz"), emit: procd_fqs
//  tuple val(pat_name), val(run), val(dataset), path("*_1_fastqc.zip"), path("*_2_fastqc.zip"), optional: true, emit: fastqc_zips

  script:
  """
  tail -n +2 ${white_list} | sed 's/-1//g' > white_list.filt

  FQ1_BUFR=`echo ${fq1}`
  FQ2_BUFR=`echo ${fq2}`

  umi_tools extract \
  --extract-method=string \
  --whitelist=white_list.filt \
  --bc-pattern=${bc_pattern} \
  -I ${fq1} \
  --read2-in=${fq2} \
  -S \${FQ1_BUFR%.fastq.gz}.bc_filtd.fastq.gz \
  --read2-out=\${FQ2_BUFR%.fastq.gz}.bc_filtd.fastq.gz \
  ${parstr}
  """
}
